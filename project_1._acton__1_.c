//������

// ��: ����� ����
// �"�: 212280911

//��: ���� ������� 
// �"�: 326945763

// ��: ���� ����
//�"�: 319034757

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SIZE 6
#define WORD 20
struct Card
{
	char* name;
	int state;//close=0,open=1
};
struct Card ** build(int *N);
struct Card** user(struct Card** arr, int size);
void print(struct Card ** game, int size);
void freeStruct(struct Card** ar, int size);


//main funciton
int main()
{
	int N,i,j,a,b,counter=0,guess=0;
	struct Card **game = NULL;
	game = build(&N);
	if (!game)
	{
		freeStruct(game, N);
		return 0;
	}
	game = user(game, N);
	if (!game)
	{
		freeStruct(game, N);
		return 0;
	}
	for (i = 0; i < N; ++i)
	{
		for (j = 0; j < SIZE; ++j)
		{
			game[i][j].state = 0;

		}

	}
	print(game, N);
	while (counter < 6 * N)
	{
		printf("Please Enter First Card:\n");
		scanf_s("%d%d", &i, &j);
		while (i<1 || i>N || j<1 || j>SIZE)
		{
			printf("Please Enter First Card:\n");
			scanf_s("%d%d", &i, &j);

		}
		game[i-1][j-1].state = 1;
		print(game, N);
		printf("Please Enter Second Card:\n");
		scanf_s("%d%d", &a, &b);
		++guess;
		while (a<1 || a>N || b<1 || b>SIZE)
		{
			printf("Please Enter Second Card:\n");
			scanf_s("%d%d", &a, &b);

		}
		game[a-1][b-1].state = 1;
		print(game, N);
		if (strcmp(game[i-1][j-1].name, game[a-1][b-1].name) == 0)
		{
			printf("Good Job!");
			print(game, N);
			printf("\n");
			++counter;
		}
		else
		{
			game[i-1][j-1].state = 0;
			game[a-1][b-1].state = 0;
			print(game, N);
			printf("\n");
		}


	}
	printf("Guesses:%d", guess);
	printf("Well Done!\n");
	freeStruct(game, N);
	return 0;
}


//A function that builds the array of structures

struct Card** build(int *N)
{
	struct Card ** arr = NULL;
	int i;
	printf("Please Enter Size Of Rows:\n");
	scanf_s("%d", N);
	while (!(*N % 2 == 0) && !(*N > 0))
	{
		printf("Error!,Please Enter New Size Of Rows:\n");//getting new size from user untill he enters a correct input.
		scanf_s("%d", N);
	}
	arr = (struct Card**)malloc(*N*sizeof(struct Card*));
	if (!arr)
	{
		return NULL;
	}
	for (i = 0; i < *N; ++i)
	{
		arr[i] = (struct Card*)malloc(SIZE * sizeof(struct Card));
		if (!arr[i])
		{
			freeStruct(arr, N);
			return NULL;
		}
		
	}
	return arr;
}




//A function that receives from the user the data for the game

struct Card** user(struct Card** arr,int size)
{
	int i, j,a,b,c,k,flag=0;
	char word[WORD];
	char** newstring;
	newstring = (char**)malloc((3 * size) * sizeof(char*));
	if (!newstring)
	{
		return NULL;
	}
	for (i = 0; i < size; ++i)
	{

		for (j = 0; j < SIZE; j++)
		{
			arr[i][j].name = NULL;
		}
	}
	for (i = 0; i < 3 * size; ++i)
	{
		printf("Please Enter a Word:\n");
		scanf("%s", word);
		flag = 0;
		for (k = 0; k < i; ++k)
		{
			if (strcmp(newstring[k], word) == 0)
			{
				--i;
				printf("Error!,Existing Word!");
				flag = 1;
				break;
			}


		}
		if (flag == 0)
		{
			newstring[k] = (char*)malloc((strlen(word) + 1) * sizeof(char));
			if (!newstring[k])
			{
				return NULL;
			}
			strcpy(newstring[k], word);
		}
	}
	 
	for (k = 0; k < 3 * size; ++k)
	{
		for (c = 0; c < 2; ++c)
		{
			a = rand() % size;
			while (a >= size || a < 0)
			{
				a = rand() % size;
			}
			b = rand() % SIZE;
			while (b >= SIZE || b < 0)
			{
				b = rand() % SIZE;
			}
			while (arr[a][b].name != NULL)
			{
				a = rand() % size;
				while (a >= size || a < 0)
				{
					a = rand() % size;
				}
				b = rand() % SIZE;
				while (b >= SIZE || b < 0)
				{
					b = rand() % SIZE;
				}
			}

			arr[a][b].name = (char*)malloc((strlen(newstring[k]) + 1) * sizeof(char));

			if (!arr[a][b].name)
			{
				return NULL;
			}
			strcpy(arr[a][b].name, newstring[k]);
		}
	}
	for (k = 0; k < 3 * size; ++k)
	{
		free(newstring[k]);
	}
	free(newstring);
	
		return arr;
}





//function that print the cards

void print(struct Card ** game, int size)
{
	int i, j;
	for (i = 0; i < size; ++i)
	{
		for (j = 0; j < SIZE; ++j)
		{
			if (game[i][j].state == 1)
			{
				printf("%s\t",game[i][j].name);
			}

			else if (game[i][j].state == 0)
			{
				printf("----");
				printf("\t");
				
			}
		}
		printf("\n");

	}
}



//free struct arr
void freeStruct(struct Card** ar, int size)
{
	int i, j;
	for (i = 0; i < size; ++i)
	{
		for (j = 0; j < SIZE; ++j)
		{
			free(ar[i][j].name);
			
		}
		free(ar[i]);
	}
	free(ar);
	return;
}

